FROM quay.io/keycloak/keycloak:18.0.2

ENV KC_HEALTH_ENABLED=true
ENV KC_METRICS_ENABLED=true

# Pass these parameters in the docker run for security purpose
#ENV KEYCLOAK_ADMIN=admin
#ENV KEYCLOAK_ADMIN_PASSWORD=changeMe

COPY /target/eu.gaiax-broker-protocol-mapper-1.1.jar /opt/keycloak/providers

EXPOSE 8080