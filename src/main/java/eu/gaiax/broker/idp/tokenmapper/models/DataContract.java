package eu.gaiax.broker.idp.tokenmapper.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.broker.idp.tokenmapper.constants.DataContractConstant;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jbosslog.JBossLog;
import okhttp3.*;
import org.apache.http.annotation.Contract;

import java.io.IOException;
import java.util.Map;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JBossLog
public class DataContract extends BaseCredential {

    @JsonProperty("credentialSubject")
    private Map<String, Object> credentialSubject;

    @JsonIgnore
    private final static ObjectMapper objectMapper = new ObjectMapper();


    private static String getContractURLFromConsumerID(String contractBaseUrl, String consumerDID){

        String contractIDFromConsumerID = consumerDID.replace("-","")
                .replace("data.json", "")
                .replace(":","")
                .replace("/","")
                .replace(".", "")
                .replace("-", "")
                .replace("@","");

        String contractURL = //DataContractConstant.CONTRACT_REPOSITORY_BASE_URL +
                contractBaseUrl +
                DataContractConstant.CONTRACT_LOCATION_SEPERATOR +
                contractIDFromConsumerID +
                DataContractConstant.CONTRACT_LOCATION_SEPERATOR +
                DataContractConstant.CONTRACT_FILE_NAME;

        log.infof("The Contract URL is: %s", contractURL);
        return contractURL;
    }


    public static  DataContract retrieve(String contractBaseUrl, String consumerDID){
        log.infof("Retrieving data contract for consumer: [%s]",consumerDID );
        String url;
        if(consumerDID == null){
            url = DataContractConstant.CONTRACT_REPOSITORY_COMPLETE_URL;
        } else {
            url = getContractURLFromConsumerID(contractBaseUrl,consumerDID);
        }
        log.infof("Build the Contract Location URL: "+url);
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .get()
                .url(url)
                .build();

        Call call = okHttpClient.newCall(request);
        try {
            Response response = call.execute();
            if(!response.isSuccessful()){
                log.errorf("Failed to retrieve the DataContract for consumerID %s", consumerDID);
                return null;
            }

            String responseBody = response.body().string();
            DataContract dataContract = objectMapper.readValue(responseBody, DataContract.class);

            return dataContract;

        } catch (IOException e) {
            log.errorf("Failed to retrieve the DataContract for consumerID %s", consumerDID);
            throw new RuntimeException(e);
        }
    }

    public DataProduct getDataProductForContract(){

        OkHttpClient okHttpClient = new OkHttpClient();
        String dataProductURI = (String)credentialSubject.get(DataContractConstant.CONTRACT_DATA_PRODUCT_CLAIM);
        log.infof("Retrieving the DataProductURI: %s", dataProductURI);
        Request request = new Request.Builder()
                .get()
                .url(dataProductURI)
                .build();

        Call call = okHttpClient.newCall(request);
        try {
            Response response = call.execute();
            if(!response.isSuccessful()){
                return null;
            }
            String responseBody = response.body().string();
            DataProduct dataProduct = objectMapper.readValue(responseBody, DataProduct.class);
            return dataProduct;
        } catch (IOException e) {

            throw new RuntimeException(e);
        }
    }

    public DataProduct getDataProductForContractWithNewTemplate() throws JsonProcessingException {
        //Object signatureSubject = credentialSubject.get(DataContractConstant.CONTRACT_DATA_PRODUCT_SIGNATURE_CLAIM);
        //Object dataProductObject = ((Map<String, Object>)signatureSubject).get(DataContractConstant.CONTRACT_DATA_PRODUCT_CLAIM);
        Object dataProductObject = credentialSubject.get(DataContractConstant.CONTRACT_DATA_PRODUCT_CLAIM);

        ObjectMapper objectMapper = new ObjectMapper();

        String serializedDataProductObject = objectMapper.writeValueAsString(dataProductObject);
        DataProduct dataProduct  = objectMapper.readValue(serializedDataProductObject, DataProduct.class);
        System.out.println(dataProduct.getId());
        return dataProduct;
    }
}