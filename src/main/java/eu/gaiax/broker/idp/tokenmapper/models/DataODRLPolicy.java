package eu.gaiax.broker.idp.tokenmapper.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataODRLPolicy {

//    @JsonProperty("@context")
//    private String context;
//
//    @JsonProperty("@type")
//    private String type;
//
//    @JsonProperty("uid")
//    private String uid;
//
//    @JsonProperty("permission")
//    private List<Map<String, Object>> permission;

    @JsonProperty("target")
    private String target;

    @JsonProperty("assignee")
    private String assignee;

    @JsonProperty("action")
    private String action;

//    @JsonProperty("duty")
//    private List<String> duty;
//
//    @JsonProperty("constraint")
//    private List<String> constraint;
}