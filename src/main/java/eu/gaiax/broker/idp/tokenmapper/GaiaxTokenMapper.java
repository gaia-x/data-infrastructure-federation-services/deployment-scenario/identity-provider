package eu.gaiax.broker.idp.tokenmapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import eu.gaiax.broker.idp.tokenmapper.models.DataContract;
import eu.gaiax.broker.idp.tokenmapper.models.DataProduct;
import lombok.extern.jbosslog.JBossLog;
import org.apache.commons.lang.StringUtils;
import org.keycloak.broker.oidc.KeycloakOIDCIdentityProviderFactory;
import org.keycloak.broker.oidc.OIDCIdentityProviderFactory;
import org.keycloak.broker.provider.AbstractIdentityProviderMapper;
import org.keycloak.broker.provider.BrokeredIdentityContext;
import org.keycloak.models.*;
import org.keycloak.provider.ProviderConfigProperty;

import java.util.*;


@JBossLog
public class GaiaxTokenMapper extends AbstractIdentityProviderMapper {

    public static final String[] COMPATIBLE_PROVIDERS = {OIDCIdentityProviderFactory.PROVIDER_ID, KeycloakOIDCIdentityProviderFactory.PROVIDER_ID};
    public static final String PROVIDER_ID = "gaia-x-idp-mapper";

    // Import or Create a new group for a user
    public static final  String GROUP_ATTRIBUTE_NAME = "gaia_x_group";
    public static final String DEFAULT_GROUP_ATTRIBUTE_NAME = "gaia_x";

    // Translating contract clauses into token claims for authorization purposes

    public static final String GAIA_X_CONTRACTING_CLAUSE = "gaia_x_clause";
    public static final String DEFAULT_GAIA_X_CONTRACTING_CLAUSE_VALUE = "readOnly";
    public static final String GAIA_X_CONTRACTING_AUTHORIZATION_DEFINITION = "gaia_x_authorization";

    // Config contract base url
    public static final String GAIA_X_CONTRACTING_BASE_URL = "gaia_x_contract_url";
    public String GAIA_X_CONTRACT_DEFAULT_BASE_URL = "https://abc-federation.gaia-x.community/participant/8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/vc/contract123/data.json";


    // Config data provider in the contract
    public static String GAIA_X_DATA_PROVIDER_NAME = "gaia_x_provider_name";
    public static String GAIA_X_DATA_PROVIDER_DEFAULT_NAME = "minio";

    public static final List<ProviderConfigProperty> CONFIG_PROPERTIES;
    private static final Set<IdentityProviderSyncMode> IDENTITY_PROVIDER_SYNC_MODES = new HashSet<>(Arrays.asList(IdentityProviderSyncMode.values()));


    static {
        List<ProviderConfigProperty> configProperties = new ArrayList<>();

//        ProviderConfigProperty property;
//        property = new ProviderConfigProperty();
//        property.setName(GROUP_ATTRIBUTE_NAME);
//        property.setLabel("Group Attribute Name Label");
//        property.setHelpText("Name of group attribute to search for in assertion.  You can leave this blank and specify a friendly name instead.");
//        property.setType(ProviderConfigProperty.STRING_TYPE);
//        property.setDefaultValue(DEFAULT_GROUP_ATTRIBUTE_NAME);

        ProviderConfigProperty dataProviderProperty;
        dataProviderProperty = new ProviderConfigProperty();
        dataProviderProperty.setName(GAIA_X_DATA_PROVIDER_NAME);
        dataProviderProperty.setLabel("Data Provider");
        dataProviderProperty.setHelpText("Name of the data provider in the gaia-x contract, You can leave it blank to accept Minio as default data provider");
        configProperties.add(dataProviderProperty);


        ProviderConfigProperty clauseProperty;
        clauseProperty = new ProviderConfigProperty();
        clauseProperty.setName(GAIA_X_CONTRACTING_CLAUSE);
        clauseProperty.setLabel("Contract Clause");
        clauseProperty.setHelpText("Value of the action, that user can perform on dataset, to search for in the contract credential");
        configProperties.add(clauseProperty);

        ProviderConfigProperty authorizationProperty;
        authorizationProperty = new ProviderConfigProperty();
        authorizationProperty.setName(GAIA_X_CONTRACTING_AUTHORIZATION_DEFINITION);
        authorizationProperty.setLabel("Data Provider Policies");
        authorizationProperty.setHelpText("Assign corresponding data usage policies to user if the above clause is present");
        configProperties.add(authorizationProperty);

        ProviderConfigProperty baseContractUrl;
        baseContractUrl = new ProviderConfigProperty();
        baseContractUrl.setName(GAIA_X_CONTRACTING_BASE_URL);
        baseContractUrl.setLabel("Contract Base URL");
        baseContractUrl.setHelpText("Configure the base url for retrieving all contracts of Provider");
        configProperties.add(baseContractUrl);

        CONFIG_PROPERTIES = Collections.unmodifiableList(configProperties);


    }
    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    @Override
    public String getDisplayCategory() {
        return "Gaia-x Contracting Token Mapper";
    }

    @Override
    public String getDisplayType() {
        return "Gaia-x Contracting Token Mapper";
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return CONFIG_PROPERTIES;
    }

    @Override
    public String[] getCompatibleProviders() {
        return COMPATIBLE_PROVIDERS;
    }

    @Override
    public String getHelpText() {
        return "Automatically extract Gaia-x contract-related attributes and convert into Gaia-x Provider permissions";
    }

    @Override
    public boolean supportsSyncMode(IdentityProviderSyncMode syncMode) {
        return IDENTITY_PROVIDER_SYNC_MODES.contains(syncMode);
    }

    @Override
    public void importNewUser(KeycloakSession session, RealmModel realm, UserModel user, IdentityProviderMapperModel mapperModel, BrokeredIdentityContext context) {
        //super.importNewUser(session, realm, user, mapperModel, context);
        log.infof("Importing a user with username: %s", user.getUsername());
        try {
            createOrUpdateContractingPolicies(realm, user, mapperModel, context);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateBrokeredUser(KeycloakSession session, RealmModel realm, UserModel user, IdentityProviderMapperModel mapperModel, BrokeredIdentityContext context) {
        //super.updateBrokeredUser(session, realm, user, mapperModel, context);
        log.infof("Updating details of a user with the username: %s ", user.getUsername());
        try {
            createOrUpdateContractingPolicies(realm, user, mapperModel, context);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }


    private  boolean isClauseActionPresentInContract(List<String> permission, String action){
        for(String p: permission){
            if(p.contains(action)){
                return true;
            }
        }

        return false;
    }

    private void createOrUpdateContractingPolicies(RealmModel realm, UserModel user, IdentityProviderMapperModel mapperModel, BrokeredIdentityContext context) throws JsonProcessingException {

        String dataProviderName = mapperModel.getConfig().getOrDefault(GAIA_X_DATA_PROVIDER_NAME, GAIA_X_DATA_PROVIDER_DEFAULT_NAME);
        dataProviderName = dataProviderName.toLowerCase();

        String contractBaseUrl = mapperModel.getConfig().getOrDefault(GAIA_X_CONTRACTING_BASE_URL, GAIA_X_CONTRACT_DEFAULT_BASE_URL);
        log.infof("Getting the Contract Base Url at [%s]", contractBaseUrl);

        if(dataProviderName.equals("minio")){
            //String consumerDID = getConsumerDIDInTokenSub(context);
            String consumerDID = user.getUsername();
            log.infof("User DID: %s", consumerDID);

            //Get all specified Action in contract
            List<String> agreedPermissionsInContract = getAllAgreedPermissionInContract(contractBaseUrl, consumerDID);

            String clauseValue =  mapperModel.getConfig().get(GAIA_X_CONTRACTING_CLAUSE);
            String compositeAuthorization = StringUtils.deleteWhitespace(mapperModel.getConfig().get(GAIA_X_CONTRACTING_AUTHORIZATION_DEFINITION));

            List<String> authorizationList = getDataProviderAuthorizationForActionClause(clauseValue, compositeAuthorization,agreedPermissionsInContract);
            List<String> existingAuthorizationList = user.getAttributes().get("policy");

            if(existingAuthorizationList != null){
                log.infof("Current authorizations in the policy claim of User %s is: %s  ", user.getUsername(), Arrays.toString(existingAuthorizationList.toArray()));
                authorizationList.addAll(existingAuthorizationList);
            }
            user.getAttributes().put("policy", authorizationList);
            log.infof("Stored authorizationList is %s", Arrays.toString(user.getAttributes().getOrDefault("policy", List.of()).toArray()));

//            if(isClauseExisted(clauseValue, context)){
//                List<String> authorizationList = getAuthorizationForClause(clauseValue, compositeAuthorization);
//                List<String> existingAuthorizationList = user.getAttributes().get("policy");
//                if(existingAuthorizationList != null){
//                    System.out.println("Current state of policy:  "+ Arrays.toString(existingAuthorizationList.toArray()));
//                    authorizationList.addAll(existingAuthorizationList);
//                }
//
//                user.getAttributes().put("policy", authorizationList);
//            }

        } else{
            log.errorf("The specified Provider [%s] is not yet supported", dataProviderName);
        }
    }

    private List<String> getAllAgreedPermissionInContract(String contractBaseUrl, String consumerDid) throws JsonProcessingException {

        //Fetch contract here
        DataContract dataContract = DataContract.retrieve(contractBaseUrl, consumerDid);
        //DataProduct dataProduct = dataContract.getDataProductForContract();
        DataProduct dataProduct = dataContract.getDataProductForContractWithNewTemplate();
        List<String> actions = dataProduct.getAllowedActionsFromDataProduct();
        log.infof("All agreed permission in the contract of the consumer Id %s:   %s", consumerDid, Arrays.toString(actions.toArray()) );
        return actions;
    }

    private List<String> getDataProviderAuthorizationForActionClause(String actionClause, String compositeAuthorization , List<String> agreedPermissionsInContract){
        List<String> convertedAuthorizationList = new ArrayList<>();
        List<String> configuredAuthorizations = Arrays.asList(StringUtils.split(compositeAuthorization, ";"));

        for(String a: agreedPermissionsInContract){
            if(a.contains(actionClause)){
                for(String configuredAuth : configuredAuthorizations){
                    String auth = a.replace(actionClause, configuredAuth);
                    convertedAuthorizationList.add(auth);
                }
            }
        }

        log.infof("Converted Authorization List: %s", Arrays.toString(convertedAuthorizationList.toArray()));
        return convertedAuthorizationList;
    }

}
