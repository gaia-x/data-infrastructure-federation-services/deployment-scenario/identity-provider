package eu.gaiax.broker.idp.tokenmapper.constants;

public interface DataContractConstant {
    String CONTRACT_DATA_PRODUCT_SIGNATURE_CLAIM = "gx-signature:subject";
    String CONTRACT_DATA_PRODUCT_CLAIM = "gx:dataProduct";
    String CONTRACT_REPOSITORY_COMPLETE_URL = "https://abc-federation.gaia-x.community/participant/8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/vc/contract123/data.json";
    String CONTRACT_REPOSITORY_BASE_URL = "https://abc-federation.gaia-x.community/participant/8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/vc";
    String CONTRACT_FILE_NAME = "data.json";
    String CONTRACT_LOCATION_SEPERATOR = "/";
    String DATA_PRODUCT_POLICY_CLAIM = "odrl:hasPolicy";
    String DATA_PRODUCT_POLICY_PERMISSION_CLAIM = "action";
    String DATA_PRODUCT_POLICY_TARGET_CLAIM = "target";
    String DATA_PRODUCT_DATASET_CLAIM = "gx:aggregationOf";
    String DATASET_AUTHORIZATION_PREFIX= "ccmc-";
}
