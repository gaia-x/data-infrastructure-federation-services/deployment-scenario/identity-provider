package eu.gaiax.broker.idp.tokenmapper.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.broker.idp.tokenmapper.constants.DataContractConstant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jbosslog.JBossLog;

import java.util.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JBossLog
public class DataProduct extends BaseCredential {

    @JsonProperty("credentialSubject")
    private Map<String, Object> credentialSubject = new HashMap<>();

    private List<DataODRLPolicy> getAllDataODRLPoliciesForDataProduct() {
        //List<DataODRLPolicy> ordlPolicies = (List<DataODRLPolicy>) credentialSubject.get(DataContractConstant.DATA_PRODUCT_POLICY_CLAIM);
        ObjectMapper objectMapper = new ObjectMapper();
        List<DataODRLPolicy> odrlPolicies;

        String body = null;
        try {
            Map<String, Object> hasPolicyObject = (Map<String, Object>) credentialSubject.get(DataContractConstant.DATA_PRODUCT_POLICY_CLAIM);
            body = objectMapper.writeValueAsString(hasPolicyObject.get("permission"));
            odrlPolicies = objectMapper.readValue(body, new TypeReference<List<DataODRLPolicy>>() {
            });

        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return odrlPolicies;
    }

    public List<String> getAllowedActionsFromDataProduct() {

        List<String> allowedActions = new ArrayList<>();

        List<DataODRLPolicy> odrlPolicies = getAllDataODRLPoliciesForDataProduct();
        List<String> dataSetIds = getDataSetIdentifierForDataProduct();

        for(DataODRLPolicy policy: odrlPolicies){
            for(String dataSetId: dataSetIds){
                String action = policy.getAction();
                String newAction = DataContractConstant.DATASET_AUTHORIZATION_PREFIX + action + "-" + dataSetId;
                allowedActions.add(newAction);
            }
        }

//        for (DataODRLPolicy dataODRLPolicy : odrlPolicies) {
//            //List<Map<String, Object>> permission = dataODRLPolicy.getPermission();
//
//            String dataSetTarget = dataODRLPolicy.getTarget();
//            int index = dataSetTarget.lastIndexOf('/');
//            String dataSetID = dataSetTarget.substring(index + 1);
//            System.out.println("DATASET ID is:  " + dataSetID);
//
//            if (dataSetIds.contains(dataSetID)) {
//                String action =  dataODRLPolicy.getAction();  //(String) p.get(DataContractConstant.DATA_PRODUCT_POLICY_PERMISSION_CLAIM);
//                String newAction = DataContractConstant.DATASET_AUTHORIZATION_PREFIX + action + "-" + dataSetID;
//                allowedActions.add(newAction);
//            }
//        }

        log.infof("All allowed actions in the contracts:   %s", Arrays.toString(allowedActions.toArray()));
        return allowedActions;
    }


    public List<String> getDataSetIdentifierForDataProduct() {
        List<String> dataSetIdentifiers = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();

        String body = null;
        try {
            body = objectMapper.writeValueAsString(credentialSubject.get(DataContractConstant.DATA_PRODUCT_DATASET_CLAIM));
            List<DataSet> dataSets = objectMapper.readValue(body, new TypeReference<List<DataSet>>() {
            });

            for (DataSet dataSet : dataSets) {
                dataSetIdentifiers.add(dataSet.getTitle());
            }
            log.infof("Obtained DataSetIdentifier: %s", Arrays.toString(dataSetIdentifiers.toArray()));
            return dataSetIdentifiers;

        } catch (JsonProcessingException e) {
            log.errorf("Failed to deserialize the DataSet from the Data Product");
            throw new RuntimeException(e);
        }
    }
}