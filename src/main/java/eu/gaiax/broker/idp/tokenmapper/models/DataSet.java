package eu.gaiax.broker.idp.tokenmapper.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataSet {

    @JsonProperty("dct:title")
    private String title;

   // @JsonProperty("dct:distributions")
    @JsonIgnore
    private List<String> distributions= new ArrayList<>();

    @JsonProperty("dct:identifier")
    private String identifier;

    //@JsonProperty("gx:exposedThrough")
    @JsonIgnore
    private List<String> exposedThrough =  new ArrayList<>();

}
