package eu.gaiax.broker.idp.tokenmapper.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseCredential {

    @JsonProperty("@context")
    private Object[] context;

    @JsonProperty("@id")
    private String id;

    @JsonProperty("@type")
    private String[] type;

    @JsonProperty("issuer")
    private String issuer;

    @JsonProperty("expirationDate")
    private String expirationDate;

    @JsonProperty("issuanceDate")
    private String issuanceDate;

    @JsonProperty("proof")
    private Object proof;
}
