package eu.gaiax.broker.idp.tokenmapper;

import eu.gaiax.broker.idp.tokenmapper.constants.DataContractConstant;
import eu.gaiax.broker.idp.tokenmapper.models.DataContract;
import lombok.Data;
import lombok.extern.jbosslog.JBossLog;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;


@JBossLog
public class Main {




    public static void main(String[] args) throws IOException {

//        File file = new File("src/main/resources/data/vc.json");
//        ObjectMapper objectMapper = new ObjectMapper();
//
//        Map<String, Object> vcDetails = objectMapper.readValue(file, Map.class);
//        List<String> keys = new ArrayList<>(vcDetails.keySet());
//        System.out.println("Keys:    "+ Arrays.toString(keys.toArray()));
//
//        DataContract contract = objectMapper.readValue(file, DataContract.class);
//        log.infof("Testing the contract %s", contract.getId());
////        System.out.println("Testing the contract:  "+ contract.getId());
//
//
//
//
//        File dataProductFile = new File("src/main/resources/data/DataProduct.json");
//        DataProduct dataProduct = objectMapper.readValue(dataProductFile, DataProduct.class);
//
//        List<DataODRLPolicy> policies= (List<DataODRLPolicy>) dataProduct.getCredentialSubject().get("gx:hasPolicy");
//        log.infof("Number of policies:    %d", policies.size());


       // DataContract.getContractURLFromConsumerID("did:web:montblanc-iot.provider.gaia-x.community:0d37a100-f679-437d-9c99-ce8758fef933:data.json");

        DataContract contract = DataContract.retrieve(DataContractConstant.CONTRACT_REPOSITORY_BASE_URL, "did:web:montblanc-iot.provider.gaia-x.community:d4a197b5-9f06-49d4-87f8-3e82551c758b:data.json");
        //List<String> actions = contract.getDataProductForContract().getAllowedActionsFromDataProduct();
        List<String> actions = contract.getDataProductForContractWithNewTemplate().getAllowedActionsFromDataProduct();
        System.out.println("Actions size: "+ actions.size());
        System.out.println("Actions are:  "+ Arrays.toString(actions.toArray()));
//        List<String> dataSetIds = contract.getDataProductForContract().getDataSetIdentifierForDataProduct();
//        System.out.println("DataSetIds : "+ Arrays.toString(dataSetIds.toArray()));
        System.out.println("Finish");
    }
}