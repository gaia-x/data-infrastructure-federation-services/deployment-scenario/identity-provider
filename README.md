## Introduction
The project aims at clarifying the authorization flow of data consuming scenarios as well as developing a Keycloak extension 
which plays the role of adaptor between permissions on dataset specified on contract and the authorization system of data Provider, i.e, Minio.

## General Architecture
The following Figure depicts the overall communication flow between the components

![image.png](/documentation/image.png)

## Data Consuming Authorization Flow
The following sequence diagram illustrates all the steps between components identified in the overall Architecture in order for a Consumer to have access et perform authorized actions on datasets declared on the previously-signed contract.

```mermaid

sequenceDiagram
  actor Consumer
   
  participant Minio (Provider)
  participant Keycloak
  participant IDPKit
  participant ABC-Checker
  participant Contract Repository

  autonumber
  activate Consumer
  activate Minio (Provider)
  Consumer ->> Minio (Provider): Consumer visits Minio's site
  Minio (Provider) -->> Consumer: Redirect Consumer to Keycloak
  deactivate Minio (Provider)

  activate Keycloak
  Consumer ->> Keycloak: Select the SSI Authentication Method
  
  Keycloak -->> Consumer: Redirect Consumer to  IDP Kit
  deactivate Keycloak
  activate IDPKit
  Consumer ->> IDPKit: Select the wallet
  IDPKit -->> Consumer: Return authorization request to User's Wallet
  Consumer ->> Consumer: Select the VCs to be submitted
  Consumer ->> IDPKit: Send Authorization Response (incl: vp_token, id_token)
  deactivate Consumer
  
  IDPKit ->> IDPKit: Perform basic verifications on VCs/VP
  activate ABC-Checker
  IDPKit ->> ABC-Checker: Send VCs
  Note left of Contract Repository: If Contract Repo does not exist, the Contract must be sent by Consumer
  
  activate Contract Repository
  ABC-Checker ->> Contract Repository: Request Contract & Data Product Description based on Consumer DID
  Contract Repository -->> ABC-Checker: Return the requested contract
  deactivate Contract Repository
  
  ABC-Checker ->> ABC-Checker: Perform Business Verification
  ABC-Checker ->> IDPKit: Return Verification Response
  deactivate ABC-Checker
  IDPKit ->> IDPKit: Extract "action" clauses & DataSet IDs in the contract
  IDPKit ->> IDPKit: Generate a new id_token_idpkit with the obtained clauses
  activate Keycloak
  IDPKit ->> Keycloak: Return the authentication response
  deactivate IDPKit
  Keycloak ->> Keycloak: Transform the contract clauses into OIDC Claims
  Keycloak ->> Keycloak: Issue id_token_kc with the generated claims
  Keycloak ->> Minio (Provider): Return the Authentication Response
  deactivate Keycloak
  activate Minio (Provider)
  Minio (Provider) ->> Minio (Provider): Perform authorization based on OIDC claims
  Minio (Provider) ->> Consumer: Return an Access Request Response to Consumer
  deactivate Minio (Provider) 

```
### Keycloak Extension

Currently, in the data exchange specification, Gaia-x relies on ODRL languague to model authorization policies applied to data products. While the languague is rich and interesting, its stil in the early stage and is not fully supported by real world applications which traditionally uses other languagues, i.e Amazon S3 IAM. Due to this reason, we develop a Keycloak extension which is in charge of the conversion between ODRL and the languague used by other applications.

In the context of the demo, we make use of MinIO, an open-source data storage project on the Data Provider side, to store all data products which are made available to authorized Data Consumers. In reality, Minio uses Amazon S3 IAM as the primary policy languague, the Keycloak extension is thus responsible for converting ODRL policies, which are included in the contract signed by both Data Provider and Data Consumer, into Amazon S3 IAM policies.


The extension provides a basic configuration template to transform clauses encoded in the contract's policies into authorization policies which can be comprehensible by Minio. The documentation provides a simple example shown below for such configuration in the Keycloak instance.

![keycloak-mapper.png](/documentation/keycloak-mapper.png)

As marked on the Figure, different fields must be provided:
- Data Provider: to specify the data provider in the contract. This is needed for the extension to determine the targeted policy languague, for example Amazon S3 IAM of MinIO
- Contract Clause: to specify the clause which needs to be converted
- Data Provider Policies: the targeted policies on the Data Provider side 
- Contract Base Url: to specify the base url of the contract repository. This helps the extension to retrieve the contract and extract ODRL policies 


### Business Verification
Whereas the basic VC/VP verifications are performed by IdPKit, ABC-Checker is in charge of all business rules.
The latter is composed of contract clauses which specify the effective data, purpose and the perimeter of data usage. For example, one of the rules can be to verify the date from which Consumer can
start using the data product.
